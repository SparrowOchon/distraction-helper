import ctypes
import win32api
import ctypes.wintypes
import win32process
import win32gui
import wmi

user32 = ctypes.windll.user32
ole32 = ctypes.windll.ole32
ole32.CoInitialize(0)
conn = wmi.WMI()
monitor_size = (win32api.GetSystemMetrics(0), win32api.GetSystemMetrics(1))
EnumWindows = ctypes.windll.user32.EnumWindows
EnumWindowsProc = ctypes.WINFUNCTYPE(ctypes.c_bool, ctypes.POINTER(ctypes.c_int), ctypes.POINTER(ctypes.c_int))
GetWindowText = ctypes.windll.user32.GetWindowTextW
GetWindowTextLength = ctypes.windll.user32.GetWindowTextLengthW
IsWindowVisible = ctypes.windll.user32.IsWindowVisible

titles = []

def get_application_list():
    EnumWindows(EnumWindowsProc(foreach_window), 0)

def foreach_window(hwnd, lParam):
    if IsWindowVisible(hwnd):
        length = GetWindowTextLength(hwnd)
        buff = ctypes.create_unicode_buffer(length + 1)
        GetWindowText(hwnd, buff, length + 1)
        if buff.value != "":
            titles.append(buff.value)
    return True

def watch_new_application(app_name):
    watcher = conn.watch_for(
        notification_type="Creation",
        wmi_class="Win32_Process",
        delay_secs=2,
    )
    while 1:
        _ = watcher()
        new_hwnd = win32gui.GetForegroundWindow()
        test_name = win32gui.GetWindowText(new_hwnd)
        if test_name == app_name:
            return new_hwnd


def is_application_alive(application_name):
    application_hwnd = win32gui.FindWindow(None, application_name)
    if application_hwnd != 0:
        return application_hwnd
    else:
        return watch_new_application(application_name)

def get_application_info(application_hwnd):
    return win32process.GetWindowThreadProcessId(application_hwnd)