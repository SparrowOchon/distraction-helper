# distraction-helper

Reminder to get back to work after leaving a full screen task. Made during a 30 minute commute to work.

## Features

- No Hooks to avoid flagging by anti-cheat as a false positive
- Fully customizable alert message
- Single Executable
- Auto close on game termination
- Background monitoring
- Automatically detect application when it starts and monitor it

## Usage

The program makes use of 3 parameters.

- Application Name: Denotes the application name to monitor as it appears in `Task Manager.`
- Game End Delay: The delay between each check for the termination (explanation in features)
- Game End Message: The message to display on the program end alert.

## Installation

The program requires no installation when using the prebuilt exec from releases.

## Manually Build

- The program uses conda to manage packages as such:
  - Build the pre-reqs:`conda create --name distraction-helper --file requirements.txt`
  - Launch the environment: `conda activate distraction-helper`
- Build the exec: `pyinstaller --noconsole --clean --onefile main.py -n distraction-helper`
