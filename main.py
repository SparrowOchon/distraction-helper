import ui
import utility
import psutil
import time
from win32con import MB_SYSTEMMODAL

if __name__ == '__main__':
    try:
        utility.get_application_list()
        ui_input = ui.GUI()
        application_hwnd = utility.is_application_alive(ui_input.application_name)
        thread_id, process_id = utility.get_application_info(application_hwnd)
        while psutil.pid_exists(process_id):
            time.sleep(int(ui_input.gameend_check_timer))
        utility.user32.MessageBoxW(0, ui_input.gameend_message, "READ THIS", MB_SYSTEMMODAL)
    except Exception as e:
        print(e)