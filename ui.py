import ctypes

import utility
from utility import titles
from tkinter import *

from win32con import MB_SYSTEMMODAL

class GUI:
    win = Tk()
    application_name = None
    gameend_message = None
    gameend_check_timer = None

    __temp_application_name = None
    __temp_gameend_message = None
    __temp_gameend_check_timer = None

    def __init__(self):
        self.__temp_application_name = StringVar(self.win)
        self.__temp_gameend_message = StringVar(self.win)
        self.__temp_gameend_check_timer = StringVar(self.win)

        self.lbl1 = Label(self.win, text='Application Name')
        self.lbl4 = Label(self.win, text='Game End Delay(Sec)')
        self.lbl5 = Label(self.win, text='Game End Message')

        self.__temp_application_name.set(titles[0])
        self.t1 = OptionMenu(self.win, self.__temp_application_name, *titles)
        self.t4 = Entry(self.win, textvariable=self.__temp_gameend_check_timer)
        self.t5 = Entry(self.win, textvariable=self.__temp_gameend_message)
        self.btn1 = Button(self.win, text='Start')

        self.lbl1.place(x=20, y=50)
        self.t1.place(x=150, y=50)
        self.lbl4.place(x=20, y=100)
        self.t4.place(x=150, y=100)
        self.lbl5.place(x=20, y=150)
        self.t5.place(x=150, y=150)

        self.b1 = Button(self.win, text='Start', command=self.validate_input, width=10)
        self.b1.place(x=100, y=200)

        self.win.title('Distraction Alarm')
        self.win.resizable(False,False)
        self.win.geometry("400x250+10+10")

        self.win.mainloop()

    def validate_input(self):
        error_message = None
        if self.__temp_application_name.get():
            self.application_name = self.__temp_application_name.get()
        else:
            error_message = "Invalid Application name."

        if self.__temp_gameend_message.get():
            self.gameend_message = self.__temp_gameend_message.get()
        else:
            error_message = "Invalid Game End Message."

        if self.__is_integer(self.__temp_gameend_check_timer.get()) is not None:
            self.gameend_check_timer = self.__temp_gameend_check_timer.get()
        else:
            error_message = "Invalid Game End Check Delay Timer. Must be a valid Integer"

        if error_message is None:
            self.win.destroy()
        else:
            self.__error_message(error_message)

    def __is_integer(self, message):
        try:
            return int(message)
        except ValueError:
            return None

    def __error_message(self, msg):
        utility.user32.MessageBoxW(0, msg, "Error", MB_SYSTEMMODAL)
